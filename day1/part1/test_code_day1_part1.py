from pytest_bdd import scenario, given, when, then, parsers
import main_day1_part1 as main


#Test1

@scenario('user_story_day1_part1.feature', 'Entering 1122')
def test():
	"""Checking the output of 1122"""

@when(parsers.parse("I enter my 1122"), target_fixture="output")
def tested_it():
	return main.solve_captcha("1122")

@then('I want the output 3')
def print_test(output):
	assert output == 3


#Test2

@scenario('user_story_day1_part1.feature', 'Entering 1111')
def test():
	"""Checking the output of 1111"""

@when(parsers.parse("I enter my 1111"), target_fixture="output")
def tested_it():
	return main.solve_captcha("1111")

@then('I want the output 4')
def print_test(output):
	assert output == 4


#Test3

@scenario('user_story_day1_part1.feature', 'Entering 91212129')
def test():
	"""Checking the output of 91212129"""

@when(parsers.parse("I enter my 91212129"), target_fixture="output")
def tested_it():
	return main.solve_captcha("91212129")

@then('I want the output 9')
def print_test(output):
	assert output == 9